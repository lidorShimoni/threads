#include "threads.h"



int main()
{
	call_I_Love_Threads();

	vector<int> primes1;
	getPrimes(58, 100, primes1);

	vector<int> primes3 = callGetPrimes(93, 289);

	callWritePrimesMultipleThreads(1, 100000, "primes2.txt", 2);

	callGetPrimes(0, 1000);
	callGetPrimes(0, 100000);
	callWritePrimesMultipleThreads(0, 1000, "1.txt", 5);
	callWritePrimesMultipleThreads(0, 100000, "2.txt", 5);

	system("pause");
	return 0;
}